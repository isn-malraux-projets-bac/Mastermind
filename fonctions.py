# -*- coding: utf-8 -*-
"""
Created on Fri Mar 24 11:17:12 2017

@author: Elyne/maëliss
"""

import sys

def parametre():
    """usage: Permet d'afficher les différents paramètres qui encadrent la future partie
    Conditions initiales: aucune 
    Arguments: n (nombre de couleur, t (nombre de trou) et c (nombre de chance)
    Sortie/retour: renvoie à la l'affichage des paramètre pouvant être modifier.
    """

    nombre_couleur= int(input("Avec quel nombre de couleur voulez-vous jouer (8 maximum) ? "))
    while nombre_couleur <= 0 :
        nombre_couleur= int(input("ENTREE NON VALIDE : Veuillez rentrer le nombre de couleurs avec lesquelles vous voulez jouer : "))

    nombre_tour= int(input("Combien d'éssaie voulez-vous ? "))
    while nombre_tour <= 0 :
        nombre_tour= int(input("ENTREE NON VALIDE : Veuillez rentrer le nombre d'éssaies que vous souhaitez : "))

    nombre_case=4 #faire boucle tant que nombre_case !=4
    print("Le jeu sera avec", nombre_case," cases")
    print()
        
    print("Signification des initiales: R = Rouge")
    print("                             V = Vert")
    print("                             B = Bleu")
    print("                             J = Jaune")
    print("                             O = Orange")
    print("                             N = Noir ")
    print("                             M = Marron")
    print("                             G = Gris")
    
    print()
        
   

def jouer():
    """
    usage: Permet de lancer la simulation de jeu (pour l'instant homme/machine)
    Conditions initiales: les paramètres doivent etre enregistres
    Arguments: 2 joueurs (machine et homme)
    Sortie/retour: renvoie l'affichage de la partie (composition du joueur 1, couleur utilisé par le joueur 2...)
    """
    print("Bienvevue et bonne partie")
    import random 
    tab_couleur = ['R', 'V', 'B', 'J','O','N','M','G']
    print("Vos couleur disponibles sont : " , tab_couleur[0:nombre_couleur])
    
    tab_machine = [tab_couleur[random.randint(0, nombre_couleur)] for couleur in range(nombre_case)]
    print(tab_machine)
    #tab_machine toujours égale à tab_couleur 
    print()
    
    tab_homme = ['.' for i in range (nombre_case)]
    print (tab_homme)
    print()
    composition=None
    compteur_tour=0
    
    while tab_homme!=tab_machine and compteur_tour<=nombre_tour:
        for couleur_homme in tab_homme:
            while composition != nombre_case:
                composition = ""
                print("Tapez l'initiale des couleurs que vous voulez rentrez parmis celles proposées : ")
                composition = input()
                print("Veuillez rentrer le bon nombre de couleur ")
           
        
                tab_resultat = [None for j in range (nombre_case)]
                tab_exclus = [False for k in range (nombre_case)]
        
                compteur_homme = 0
                for couleur_homme in tab_homme:    
                    if tab_machine[compteur_homme] == couleur_homme:
                        tab_resultat[compteur_homme] = 'CP'
                        tab_exclus[compteur_homme] = True
                        compteur_homme += 1
    
                print(tab_resultat)
                print(tab_exclus)
                print("---------------")
        
                for couleur_homme in tab_homme:
                    compteur_machine = 0
                    for couleur_machine in tab_machine:
                        if compteur_machine != compteur_homme:
                            if tab_exclus[compteur_machine] == False:
                                if tab_resultat[compteur_homme] is None:
                                    if tab_machine[compteur_machine] == couleur_homme:
                                        tab_resultat[compteur_homme] = 'CP!'
                                        tab_exclus[compteur_machine] = True
                                        # arreter la boucle for sur tab_machine
                                        break
                                    compteur_machine += 1
                compteur_homme += 1
    
                print(tab_resultat)
                print(tab_exclus)
                
                compteur_tour+=1
    
    print("Vous avez gagnez en ", compteur_tour)


def nouvelle_partie():
    """usage: Permet de generer une nouvelle partie directement
    Conditions initiales: une partie doit déjà être lançee
    Arguments: aucun
    Sortie/retour: renvoie à reinitialiser une partie.
    """
    pass

def quitter():
    """usage: Permet de quitter le jeu 
    Conditions initiales: le jeu doit être ouvert 
    Arguments: aucun
    Sortie/retour: renvoie à quitter le programme donc quitter le jeu.
    """
    sys.exit(0)