# -*- coding: utf-8 -*-
"""
Created on Fri Mar 24 10:42:26 2017

@author: elyne/maëliss
structure (squellette) du programme
"""
from fonctions import parametre
from fonctions import jouer
from fonctions import quitter

def menu_general():
    """
    usage: permet d'afficher le menu c'est-à-dire les 3 propositions (paramètre, jouer, quitter)
    Conditions initiales: aucune
    Arguments: aucun
    Sortie/retour: renvoie les 3 propositions  à sélectionner.
    
    
    """
    choix = None
    while choix not in ['q', 'Q'] :
        print("- P -   Paramètres")
        print("- J -   Jouer")
        print("- Q -   Quitter")
        choix = str(input("Quel choix ? "))
        if choix in ['P', 'p'] :
            parametre()
            print(parametre)
        elif choix in ['J', 'j'] :
           jouer()
           print(jouer)
    print("Vous avez choisi de quitter le programme")
    quitter()


if __name__ == "__main__":
    menu_general()
    