print("Rentrez les différents paramètres.")
print()

nombre_couleur= int(input("Avec quel nombre de couleur voulez-vous jouer (8 maximum) ? "))
while nombre_couleur <= 0 or nombre_couleur > 8 :
    nombre_couleur= int(input("ENTREE NON VALIDE : Veuillez rentrer le nombre de couleurs avec lesquelles vous voulez jouer : "))

print()

nombre_case = 4 # faire une boucle tant que nombre_case != 4
print("Vous jouez avec" , nombre_case , "cases !" )

print()
nombre_tour= int(input("Combien d'essais voulez-vous ? "))
while nombre_tour <= 0 :
    nombre_tour= int(input("ENTREE NON VALIDE : Veuillez rentrer le nombre d'essais que vous souhaitez : "))

print()
print("Signification des initiales: \n\tR = Rouge")
print("\tV = Vert")
print("\tB = Bleu")
print("\tJ = Jaune")
print("\tO = Orange")
print("\tN = Noir ")
print("\tM = Marron")
print("\tG = Gris")

print()

print("Signification des indications :\n\tBP = Couleur bien placée ")   
print("\tMP = Couleur mal placée")
print("\t x = Couleur absente dans la combinaison")

print()

print("REGLES: A chaque essai, il faut rentrer l'initiale des couleurs \n        que vous voulez rentrer parmis celles proposées.")
print("\tTapez Q pour quitter le jeu. ")
print("\tIl est possible que la composition aléatoire de l'odinateur \n        possède les mêmes couleurs (2, 3 ou 4 fois la même).")

print()
print("Bienvenue et bonne partie !")
print()

import random
import sys 
tab_couleur = ['R', 'V', 'B', 'J','O','N','M','G']
print("Vos couleurs disponibles sont : " , tab_couleur[0:nombre_couleur])

tab_machine = [tab_couleur[random.randint(0, nombre_couleur - 1)] for couleur in range(nombre_case)]


print()
tab_homme = ['x' for i in range (nombre_case)]

composition = str()
compteur_tour=0

while tab_homme != tab_machine and compteur_tour <= nombre_tour-1:
    tab_homme_temporaire = []
    while len(tab_homme_temporaire) != nombre_case:
        composition = str(input("Essai n°" + str(compteur_tour+1) + " : "))
        print(composition)
        if composition == "Q":
            print("Vous allez quitter le jeu")
            sys.exit(0)

        tab_homme_temporaire = [c for c in composition if c in tab_couleur[0:nombre_couleur]]
    tab_homme = tab_homme_temporaire
    
    tab_resultat = ["x" for j in range (nombre_case)]
    tab_exclus = [False for k in range (nombre_case)]
        
    compteur_homme = 0
    for couleur_homme in tab_homme:    
        if tab_machine[compteur_homme] == couleur_homme:
            tab_resultat[compteur_homme] = 'BP'
            tab_exclus[compteur_homme] = True
        compteur_homme += 1
    
    compteur_homme = 0
    for couleur_homme in tab_homme:
        compteur_machine = 0
        for couleur_machine in tab_machine:
            if compteur_machine != compteur_homme:
                if tab_exclus[compteur_machine] == False:
                    if tab_resultat[compteur_homme] is "x":
                        if tab_machine[compteur_machine] == couleur_homme:
                            tab_resultat[compteur_homme] = 'MP'
                            tab_exclus[compteur_machine] = True
                            # arreter la boucle for sur tab_machine
                            break
            compteur_machine += 1
        compteur_homme += 1
    print(tab_resultat)
    
    compteur_tour+=1
    
    print()
     
if tab_homme == tab_machine :
    print("Vous avez gagné en ",compteur_tour, "coups !")
else:
    print("Vous avez perdu !")
    print("La combinaison était : ", tab_machine)    
     
print()